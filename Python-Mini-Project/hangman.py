import random
from words import word_list


class Hangman():
    ''' This is the Hangman class where it's every functionality has 
        been divided into smaller modular functions which has been
        called accordingly'''

    def __init__(self):
        self.word = random.choice(word_list)  # ? selected random string
        # generating the list of unnderscore for len of each word
        self.display = ["_" for letter in self.word] # ["_","_","_","_" ]
        self.attempts = 0
        self. max_attempts = 6
        self.inputs = []

    def show(self):
        """ Displays the attempts_left, display string, letters_required """
        # This is the string version display list
        display = (' '.join(self.display))
        letters_required = display.count("_")
        attempts_left = self.max_attempts - self.attempts
        print(
            f"""Attempts left:  [{attempts_left}], The word is: {display}  [{letters_required} letters required]""")

    def validate_char(self, guess: str) -> bool:
        """" Check for characters"""
        return not guess.isalpha()

    def get_used_chars(self, guess: str) -> bool:
        """" Check for already used characters"""
        return guess in self.inputs

    def check_guess(self, guess: str):
        # casting guess into lowercase
        guess = guess.lower()

        # break if the guess character is not a alphabet
        if self.validate_char(guess):
            print("Invalid letter ! Only alphabets are allowed")
            return

        # break if the guess character is already used
        if self.get_used_chars(guess):
            print("Letter already used")
            return

        # append new guess character into the inputs list
        self.inputs.append(guess)

        # if whole string matches
        if len(guess) > 1:
            if guess == self.word:
                self.display = self.word
            else:
                # incremet the number of attempts
                self.attempts += 1
                print(
                    f'Wrong Guess! {self.display_hangman(self.max_attempts - self.attempts)}')
        else:
            if guess in self.word:
                self.update(guess)
            else:
                # incremet the number of attempts
                self.attempts += 1
                print(
                    f'Wrong Guess! {self.display_hangman(self.max_attempts - self.attempts)}')

    def get_word_index(self, letter): #p
        """" return list of indexes where the character
         exists in self.word using list comprehension"""
        # i.e "a p p l e" , _ '_' '_' _ _ _  "p"
        return [index for index, char in enumerate(list(self.word)) if char == letter]

    def update(self, letter: str):
        # getting the index of letter from self.get_word_index
        index = self.get_word_index(letter)  # letter  = p, [1,2]
        for n in index:
            self.display[n] = letter  # [_ _ _ _ _] -> [_ p p _ _]

    def check_for_win(self) -> bool:
        """" return True if self.display == self.word 
        i.e user input string == random word """

        display = (''.join(self.display))
        return display == self.word

    def display_hangman(self, index: int):
        stages = [
            # final state: head, torso, both arms, and both legs
            """
                   --------
                   |      |
                   |      0
                   |     \\|/
                   |      |
                   |     / \\
            -------------
                """,
            # head, torso, both arms, and one leg
            """
                   --------
                   |      |
                   |      0
                   |     \\|/
                   |      |
                   |     /
            -------------
                """,
            # head, torso, and both arms
            """
                   --------
                   |      |
                   |      0
                   |     \\|/
                   |      |
                   |
            -------------
                """,
            # head, torso, and one arm
            """
                   --------
                   |      |
                   |      0
                   |     \\|
                   |      |
                   |
            -------------
                """,
            # head and torso
            """
                   --------
                   |      |
                   |      0
                   |      |
                   |      |
                   |
            -------------
                """,
            # head
            """
                   --------
                   |      |
                   |      O
                   |
                   |
                   |
            -------------
                """,
            # initial empty state
            """
                   --------
                   |      |
                   |
                   |
                   |
                   |
            -------------
                """
        ]
        return stages[index]

    def get_points(self):
        """" Generates points for user based on the number of attempts taken to solve the problem """

        if self.attempts <= 2:
            return 100
        elif self.attempts <= 3:
            return 80
        elif self.attempts <= 4:
            return 50
        elif self.attempts <= 5:
            return 30
        return 0


def game():
    # hangman object
    my_game = Hangman()

    # infinite loop
    while True:
        my_game.show()
        print()

        # user input
        word = input("Enter guess Word / letter: ")
        my_game.check_guess(word)

        if(my_game.attempts < my_game.max_attempts):
            if my_game.check_for_win():
                my_game.show()
                print()
                print("Hurray!! You Won!!")
                print(f"You took {my_game.attempts} wrong attempts")
                print(f"You got {my_game.get_points()} / 100 points")
                break
        else:
            print()
            print("||-----Game Over ------||")
            print(f"The word was '{my_game.word}'")
            print(f"You got {my_game.get_points()} / 100 points")
            break


def lets_play():
    while True:
        print()
        decision = input("Do you want to play the game? [Yes/No]: ")
        if decision.lower() == "no" or decision.lower() == "n":
            print("Thank You !")
            break
        game()


lets_play()
